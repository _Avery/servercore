package net.extasty.servercore.eventlisteners;

import net.extasty.servercore.ServerCore;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 2019-06-15 um 19:03
 **/
public class PlayerJoinListener implements Listener {

    private final ServerCore serverCore;

    public PlayerJoinListener(ServerCore serverCore) {
        this.serverCore = serverCore;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerJoin(final PlayerJoinEvent event) {
        final Player player = event.getPlayer();

        event.setJoinMessage(null);

        serverCore.getBackendManager().createPlayer(player, user -> {
            System.out.println("putted");
            System.out.println(serverCore.getBackendManager().getUsers().get(player.getUniqueId()));
        });

        serverCore.getBackendManager().createSettings(player, settings -> {
        });
    }
}

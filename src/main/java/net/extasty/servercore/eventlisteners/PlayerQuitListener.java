package net.extasty.servercore.eventlisteners;

import net.extasty.servercore.ServerCore;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 2019-06-15 um 19:03
 **/
public class PlayerQuitListener implements Listener {

    private final ServerCore serverCore;

    public PlayerQuitListener(ServerCore serverCore) {
        this.serverCore = serverCore;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerQuit(final PlayerQuitEvent event) {
        final Player player = event.getPlayer();

        event.setQuitMessage(null);

        serverCore.getBackendManager().updateUser(player.getUniqueId().toString());
        serverCore.getBackendManager().updateSettings(player.getUniqueId().toString());

        serverCore.getBackendManager().getUsers().remove(player.getUniqueId());
        serverCore.getBackendManager().getSettings().remove(player.getUniqueId());
    }
}

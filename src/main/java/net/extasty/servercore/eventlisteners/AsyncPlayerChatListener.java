package net.extasty.servercore.eventlisteners;

import de.dytanic.cloudnet.api.CloudAPI;
import net.extasty.servercore.ServerCore;
import net.extasty.servercore.misc.PermissionGroup;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 2019-06-15 um 19:15
 **/
public class AsyncPlayerChatListener implements Listener {

    private final ServerCore serverCore;

    public AsyncPlayerChatListener(ServerCore serverCore) {
        this.serverCore = serverCore;
    }

    @EventHandler
    public void onAsyncPlayerChat(final AsyncPlayerChatEvent event) {
        final Player player = event.getPlayer();

        final String rank = CloudAPI.getInstance().getOnlinePlayer(player.getUniqueId()).getPermissionEntity().getGroups().iterator().next().getGroup();
        final PermissionGroup permissionGroup = PermissionGroup.valueOf(rank.toUpperCase());

        event.setFormat("§8× " + permissionGroup.getChatPrefix() +
                permissionGroup.getColor() + player.getName() + " §8» §7" + event.getMessage());
        player.playSound(player.getLocation(), Sound.CHICKEN_EGG_POP, 1, 1);
    }
}
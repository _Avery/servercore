package net.extasty.servercore.managers.spectator;

import com.google.common.collect.Lists;
import net.extasty.servercore.ServerCore;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 2019-07-22 um 10:39
 **/
public class SpectatorManager {

    private final ServerCore serverCore;

    private final List<Player> spectators = Lists.newArrayList();

    public SpectatorManager(ServerCore serverCore) {
        this.serverCore = serverCore;
    }

    public void addSpectator(Player player) {
        this.spectators.add(player);

        for (Player online : Bukkit.getOnlinePlayers()) {
            if ((!isSpectator(online)) && (online.canSee(player))) {
                online.hidePlayer(player);
            }

            if (!player.canSee(online)) {
                player.showPlayer(online);
            }
        }

        player.setAllowFlight(true);
        player.setFlying(true);
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
    }

    public void removeSpectator(Player player) {
        this.spectators.remove(player);

        for (Player online : Bukkit.getOnlinePlayers()) {
            online.showPlayer(player);
        }

        player.setFlying(false);
        player.setAllowFlight(false);
    }

    public List<Player> getSpectators() {
        return this.spectators;
    }

    public boolean isSpectator(Player player) {
        return getSpectators().contains(player);
    }
}

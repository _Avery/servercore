package net.extasty.servercore.managers.game;

import lombok.Getter;
import lombok.Setter;
import net.extasty.servercore.template.KitTemplate;
import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 2019-07-20 um 19:39
 **/

@Getter
public class KitManager {

    private final ArrayList<KitTemplate> kits;

    @Setter
    private KitTemplate currentKit;

    public KitManager() {
        this.kits = new ArrayList<>();
    }

    /**
     * Add a kit to the list
     *
     * @param kitTemplate
     */
    public void addKit(final KitTemplate kitTemplate) {
        this.kits.add(kitTemplate);
    }

    /**
     * Remove a kit from the list
     *
     * @param kitTemplate
     */
    public void removeKit(final KitTemplate kitTemplate) {
        this.kits.remove(kitTemplate);
    }

    /**
     * This method returns a kit from a name
     *
     * @param name
     * @return
     */
    public KitTemplate getKit(final String name) {

        for (KitTemplate kit : this.kits) {
            if (kit.getName().equalsIgnoreCase(name)) {
                return kit;
            }
        }

        return null;
    }

    /**
     * This method sets the kit`s items for a player
     *
     * @param player
     */
    public void setKit(final Player player) {
        final KitTemplate kit = getCurrentKit();

        kit.setPlayerItems(player);
    }
}

package net.extasty.servercore.managers;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import lombok.Data;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 2019-06-14 um 21:43
 **/

@Data
public class MongoManager {

    private final String host;
    private final String databaseName;
    private String userName;
    private String password;
    private final int port;
    private MongoCredential credentials;
    private MongoClient mongoClient;
    private MongoDatabase mongoDatabase;
    private final ExecutorService executorService;
    private MongoCollection<Document> players;
    private MongoCollection<Document> friends;
    private MongoCollection<Document> clans;
    private MongoCollection<Document> settings;

    /**
     * initialize mongomanager
     *
     * @param host
     * @param databaseName
     * @param port
     */
    public MongoManager(String host, String databaseName, int port) {
        this.host = host;
        this.port = port;
        this.databaseName = databaseName;
        this.executorService = Executors.newSingleThreadScheduledExecutor();
    }

    /**
     * initialize mongomanager
     *
     * @param host
     * @param databaseName
     * @param port
     * @param userName
     * @param password
     */
    public MongoManager(String host, String databaseName, int port, String userName, String password) {
        this.host = host;
        this.port = port;
        this.userName = userName;
        this.password = password;
        this.databaseName = databaseName;
        this.executorService = Executors.newSingleThreadScheduledExecutor();
    }

    /**
     * Connect to database
     *
     * @param callback
     */
    public void connect(Consumer<Boolean> callback) {
        ServerAddress address = new ServerAddress(this.host, this.port);

        if (!this.userName.isEmpty() && !this.password.isEmpty()) {
            this.credentials = MongoCredential.createCredential(this.userName, "admin", this.password.toCharArray());
        }

        this.mongoClient = this.userName != null && this.password != null ? new MongoClient(address, Arrays.asList(this.credentials)) : new MongoClient(address);
        this.mongoDatabase = this.mongoClient.getDatabase(this.databaseName);

        callback.accept(true);

        this.players = this.mongoDatabase.getCollection("players");
        this.friends = this.mongoDatabase.getCollection("friends");
        this.clans = this.mongoDatabase.getCollection("clans");
        this.settings = this.mongoDatabase.getCollection("settings");
    }

    /**
     * Find a document in the database sync by using a filter
     *
     * @param collection
     * @param filter
     * @return @{@link Document}
     */
    public Document findDocumentSync(MongoCollection<Document> collection, Bson filter) {
        return (Document) collection.find(filter).first();
    }

    /**
     * Find a document in the database async, by using a filter and return the result as consumer (@{@link Document})
     *
     * @param collection
     * @param filter
     * @param callback
     */
    public void findDocumentAsync(MongoCollection<Document> collection, Bson filter, Consumer<Document> callback) {
        this.runAsync(() -> {
            callback.accept(this.findDocumentSync(collection, filter));
        });
    }

    /**
     * Find the first document in the database sync
     *
     * @param collection
     * @return @{@link Document}
     */
    public Document findFirstDocumentSync(MongoCollection<Document> collection) {
        return (Document) collection.find().first();
    }

    /**
     * Find the first document in the database async, and return the result as a consumer (@{@link Document})
     *
     * @param collection
     * @param callback
     */
    public void findFirstDocumentAsync(MongoCollection<Document> collection, Consumer<Document> callback) {
        this.runAsync(() -> {
            callback.accept(this.findFirstDocumentSync(collection));
        });
    }

    /**
     * Find the one document using a filter and update the document sync
     *
     * @param collection
     * @param filter
     * @param toUpdate
     * @return @{@link Document}
     */
    public Document findOneAndUpdateDocumentSync(MongoCollection<Document> collection, Bson filter, Document toUpdate) {
        return (Document) collection.findOneAndUpdate(filter, new Document("$set", toUpdate));
    }

    /**
     * Find the one document using a filter and update the document async, return the result as consumer (@{@link Document})
     *
     * @param collection
     * @param filter
     * @param toUpdate
     * @param callback
     */
    public void findOneAndUpdateDocumentAsync(MongoCollection<Document> collection, Bson filter, Document toUpdate, Consumer<Document> callback) {
        this.runAsync(() -> {
            callback.accept(this.findOneAndUpdateDocumentSync(collection, filter, toUpdate));
        });
    }

    /**
     * Insert a document into the database sync
     *
     * @param collection
     * @param document
     */
    public void insertDocumentSync(MongoCollection<Document> collection, Document document) {
        collection.insertOne(document);
    }

    /**
     * Insert a document into the database async
     *
     * @param collection
     * @param document
     */
    public void insertDocumentAsync(MongoCollection<Document> collection, Document document) {
        this.runAsync(() -> {
            this.insertDocumentSync(collection, document);
        });
    }

    /**
     * Delete a document in the database sync
     *
     * @param collection
     * @param filter
     * @return @{@link DeleteResult}
     */
    public DeleteResult deleteDocumentSync(MongoCollection<Document> collection, Bson filter) {
        return collection.deleteOne(filter);
    }

    /**
     * Delete a document in the database anync and return the result as consumer (@{@link DeleteResult})
     *
     * @param collection
     * @param filter
     * @param callback
     */
    public void deleteDocumentAsync(MongoCollection<Document> collection, Bson filter, Consumer<DeleteResult> callback) {
        this.runAsync(() -> {
            callback.accept(this.deleteDocumentSync(collection, filter));
        });
    }

    /**
     * Method to run a runnable async
     *
     * @param runnable
     */
    private void runAsync(Runnable runnable) {
        this.executorService.execute(runnable);
    }
}

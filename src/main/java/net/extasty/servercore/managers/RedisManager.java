package net.extasty.servercore.managers;

import lombok.Getter;
import net.extasty.servercore.ServerCore;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 06.08.2019 um 20:35
 **/
public class RedisManager {

    private final ServerCore serverCore;

    @Getter
    private JedisPool pool;

    public RedisManager(ServerCore serverCore) {
        this.serverCore = serverCore;
    }

    /**
     *
     */
    public void connect() {
        pool = new JedisPool(new JedisPoolConfig(), "localhost", 6379);
        System.out.println("jedis connected");
    }

    /**
     * Cache a player into redis database
     *
     * @param uuid means the uuid of a player
     * @param name means the name of a player
     */
    public void cachePlayer(String uuid, String name) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(0);

            jedis.hset("uuids", uuid, name);
            jedis.hset("names", name.toUpperCase(), uuid);

            if (!coinsExists(uuid)) {
                jedis.hset("coins", uuid, String.valueOf(0));
            }
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Delete a user from the cache
     *
     * @param uuid means the uuid of a player
     * @param name means the name of a player
     */
    public void deletePlayer(String uuid, String name) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(0);

            jedis.hdel("names", name.toUpperCase());
            jedis.hdel("uuids", uuid);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Get a name by uuid
     *
     * @param uuid means the uuid of a player
     * @return @{@link String}
     */
    public String getName(String uuid) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(0);
            return jedis.hget("uuids", uuid);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Get a uuid by name
     *
     * @param name means the name of a player
     * @return @{@link String}
     */
    public String getUUID(String name) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(0);
            return jedis.hget("names", name.toUpperCase());
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Check if a name exists in the database
     *
     * @param name means the name of a player
     * @return @{@link Boolean}
     */
    public boolean nameExists(String name) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(0);
            return jedis.hexists("names", name.toUpperCase());
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Check if a uuid exists in the database
     *
     * @param uuid means the uuid of a player
     * @return @{@link Boolean}
     */
    public boolean uuidExists(String uuid) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(0);
            return jedis.hexists("uuids", uuid);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Check if a user exists in the coins cache
     *
     * @param uuid means the uuid of a player
     * @return @{@link Boolean}
     */
    public boolean coinsExists(String uuid) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(0);
            return jedis.hexists("coins", uuid);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Get the cached coins from a user by given uuid
     *
     * @param uuid means the uuid of a player
     * @return @{@link Integer}
     */
    public Integer getCoins(String uuid) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(0);

            if (jedis.hget("coins", uuid) == null || jedis.hget("coins", uuid).equalsIgnoreCase("")) {
                return serverCore.getBackendManager().getCoins(uuid);
            }
            return Integer.valueOf(jedis.hget("coins", uuid));
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Set Coins of a player in the cache
     *
     * @param uuid  means the uuid of a player
     * @param coins means the coins to set
     */
    public void setCoins(String uuid, Integer coins) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(0);

            if (coinsExists(uuid)) {
                jedis.hset("coins", uuid, String.valueOf(coins));

                jedis.publish("coinsUpdate", String.valueOf(coins) + ":" + uuid);
            }
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Add coins to a player in the cache
     *
     * @param uuid  means the uuid of a player
     * @param coins means the coins to add
     */
    public void addCoins(String uuid, Integer coins) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(0);

            if (coinsExists(uuid)) {
                Integer currentCoins = Integer.valueOf(jedis.hget("coins", uuid));
                Integer addCoins = currentCoins + coins;
                jedis.hset("coins", uuid, String.valueOf(addCoins));

                jedis.publish("coinsUpdate", String.valueOf(addCoins) + ":" + uuid);
            }
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Remove coins from a player in the cache
     *
     * @param uuid  means the uuid of a player
     * @param coins means the coins to remove
     */
    public void removeCoins(String uuid, Integer coins) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(0);

            if (coinsExists(uuid)) {
                Integer currentCoins = Integer.valueOf(jedis.hget("coins", uuid));
                Integer removeCoins = currentCoins - coins;
                jedis.hset("coins", uuid, String.valueOf(removeCoins));

                jedis.publish("coinsUpdate", String.valueOf(removeCoins) + ":" + uuid);
            }
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Nick a player
     *
     * @param uuid     means the uuid of a player
     * @param nickname means the nickname to set
     */
    public void nickPlayer(String uuid, String nickname) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(0);

            jedis.hset("nickedPlayers", uuid, nickname);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Unnick a player
     *
     * @param uuid means the uuid of a player
     */
    public void unnickPlayer(String uuid) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(0);

            jedis.hdel("nickedPlayers", uuid);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Check if a player is nicked
     *
     * @param name means the name of a player
     * @return @{@link Boolean}
     */
    public boolean isNicked(String name) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(0);

            return jedis.hexists("nickedPlayers", name);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Get all nicked players
     *
     * @return @{@link Map}
     */
    public Map<String, String> getNickedPlayers() {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(0);

            return jedis.hgetAll("nickedPlayers");
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Add nickname to the nickname list
     *
     * @param nickname means the nickname to add
     */
    public void addNickname(String nickname) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(0);

            jedis.lpush("nicknames", nickname);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Remove a nickname from the nickname list
     *
     * @param nickname means the nickname to remove
     */
    public void removeNickname(String nickname) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(0);

            jedis.lrem("nicknames", 0, nickname);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Check if a nickname exists in the nickname list
     *
     * @param nickname means the nickname for check
     * @return @{@link Boolean}
     */
    public boolean nicknameExists(String nickname) {
        return getNicknames().contains(nickname);
    }

    /**
     * Get all nicknames from the list
     *
     * @return @{@link List}
     */
    public List<String> getNicknames() {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(0);

            return jedis.lrange("nicknames", 0, -1);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Enable auto nick
     *
     * @param uuid means the uuid of a player
     */
    public void enableAutoNick(String uuid) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(0);

            jedis.hset("autonick", uuid, "TRUE");
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Disable auto nick
     *
     * @param uuid means the uuid of a player
     */
    public void disableAutoNick(String uuid) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(0);

            jedis.hset("autonick", uuid, "FALSE");
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Check if a player has auto nick enabled
     *
     * @param uuid means the uuid of a player
     * @return @{@link Boolean}
     */
    public boolean hasAutoNickEnabled(String uuid) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(0);

            boolean isAutoNick = false;

            if (jedis.hget("autonick", uuid).equalsIgnoreCase("TRUE")) {
                isAutoNick = true;
            }
            return isAutoNick;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Remove a player from the coins cache
     *
     * @param uuid means the uuid of a player
     */
    public void removeFromCoins(String uuid) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(0);

            jedis.hdel("coins", uuid);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }
}

package net.extasty.servercore.managers;

import com.google.common.collect.Maps;
import com.mongodb.client.model.Filters;
import lombok.Data;
import net.extasty.servercore.ServerCore;
import net.extasty.servercore.misc.entities.Settings;
import net.extasty.servercore.misc.entities.User;
import org.bson.Document;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;
import java.util.function.Consumer;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 2019-06-14 um 21:42
 **/

@Data
public class BackendManager {

    private final ServerCore serverCore;

    private final HashMap<UUID, User> users = Maps.newHashMap();
    private final HashMap<UUID, Settings> settings = Maps.newHashMap();

    public BackendManager(ServerCore serverCore) {
        this.serverCore = serverCore;
    }

    /**
     * Create a player
     *
     * @param player
     * @param callback
     */
    public void createPlayer(final Player player, final Consumer<User> callback) {
        final User user = new User();

        if (!this.existsPlayer(player.getUniqueId().toString())) {

            user.setUuid(player.getUniqueId().toString());
            user.setName(player.getName());
            user.setIpAddress(player.getAddress().getAddress().getHostAddress());
            user.setBanPoints(0);
            user.setMutePoints(0);
            user.setCoins(1000L);
            user.setCreatedAt(System.currentTimeMillis());
            user.setLastJoin(System.currentTimeMillis());
            user.setOnlineTime(0L);
            user.setNotify(false);
            user.setDataPrivacy(false);
            user.setClanID("");

            final Document userDoc = serverCore.getGson().fromJson(serverCore.getGson().toJson(user), Document.class);
            ServerCore.getInstance().getMongoManager().insertDocumentAsync(ServerCore.getInstance().getMongoManager().getPlayers(), userDoc);

            this.users.put(UUID.fromString(user.getUuid()), user);
            callback.accept(user);
        } else {

            ServerCore.getInstance().getMongoManager().findDocumentAsync(ServerCore.getInstance().getMongoManager().getPlayers(), Filters.eq("uuid", player.getUniqueId().toString()), (userDoc) -> {

                user.setUuid(userDoc.getString("uuid"));
                user.setName(player.getName());
                user.setIpAddress(player.getAddress().getAddress().getHostAddress());
                user.setBanPoints(userDoc.getDouble("banPoints").intValue());
                user.setMutePoints(userDoc.getDouble("mutePoints").intValue());
                user.setCoins(userDoc.getDouble("coins").longValue());
                user.setCreatedAt(userDoc.getDouble("createdAt").longValue());
                user.setLastJoin(System.currentTimeMillis());
                user.setOnlineTime(userDoc.getDouble("onlineTime").longValue());
                user.setNotify(userDoc.getBoolean("notify"));
                user.setDataPrivacy(userDoc.getBoolean("dataPrivacy"));
                user.setClanID(userDoc.getString("clanID"));

                this.users.put(UUID.fromString(user.getUuid()), user);
                callback.accept(user);
            });
        }
    }

    /**
     * Create settings of a player
     *
     * @param player
     * @param callback
     */
    public void createSettings(final Player player, final Consumer<Settings> callback) {
        final Settings settings = new Settings();

        if (ServerCore.getInstance().getMongoManager().getSettings().find(Filters.eq("uuid", player.getUniqueId().toString())).first() == null) {
            settings.setUuid(player.getUniqueId().toString());
            settings.setAutoNick(false);
            settings.setDoubleJump(false);
            settings.setJumpPlates(false);
            settings.setParticles(false);
            settings.setVisibility(0);

            final Document settingsDoc = serverCore.getGson().fromJson(serverCore.getGson().toJson(settings), Document.class);
            ServerCore.getInstance().getMongoManager().insertDocumentAsync(ServerCore.getInstance().getMongoManager().getSettings(), settingsDoc);

            this.settings.put(UUID.fromString(settings.getUuid()), settings);
            callback.accept(settings);
        } else {
            ServerCore.getInstance().getMongoManager().findDocumentAsync(ServerCore.getInstance().getMongoManager().getSettings(), Filters.eq("uuid", player.getUniqueId().toString()), document -> {
                settings.setUuid(document.getString("uuid"));
                settings.setAutoNick(document.getBoolean("autoNick"));
                settings.setDoubleJump(document.getBoolean("doubleJump"));
                settings.setJumpPlates(document.getBoolean("jumpPlates"));
                settings.setParticles(document.getBoolean("particles"));
                settings.setVisibility(document.getDouble("visibility").intValue());

                this.settings.put(UUID.fromString(settings.getUuid()), settings);
                callback.accept(settings);
            });
        }
    }

    /**
     * Get the settings of a player
     *
     * @param uuid
     * @return @{@link Settings}
     */
    public Settings getSettings(final String uuid) {
        if (this.settings.get(UUID.fromString(uuid)) != null) {
            return this.settings.get(UUID.fromString(uuid));
        }

        final Document settingsDocument = ServerCore.getInstance().getMongoManager().getSettings().find(Filters.eq("uuid", uuid)).first();

        if (settingsDocument != null) {
            return serverCore.getGson().fromJson(settingsDocument.toJson(), Settings.class);
        }

        return null;
    }

    /**
     * Update a user
     *
     * @param uuid
     */
    public void updateUser(final String uuid) {
        final User user = this.getUser(uuid);

        final Document toUpdate = serverCore.getGson().fromJson(serverCore.getGson().toJson(user), Document.class);

        ServerCore.getInstance().getMongoManager().findOneAndUpdateDocumentAsync(ServerCore.getInstance().getMongoManager().getPlayers(), Filters.eq("uuid", uuid), toUpdate, document -> {
        });
    }

    /**
     * Update settings
     *
     * @param uuid
     */
    public void updateSettings(final String uuid) {
        final Settings settings = this.getSettings(uuid);

        final Document toUpdate = serverCore.getGson().fromJson(serverCore.getGson().toJson(settings), Document.class);

        ServerCore.getInstance().getMongoManager().findOneAndUpdateDocumentAsync(ServerCore.getInstance().getMongoManager().getSettings(), Filters.eq("uuid", uuid), toUpdate, document -> {
        });
    }

    /**
     * Get a user from database or cache
     *
     * @param uuid
     * @return @{@link User}
     */
    public User getUser(String uuid) {
        if (this.users.get(UUID.fromString(uuid)) != null) {
            return this.users.get(UUID.fromString(uuid));
        } else {
            Document playerDocument = ServerCore.getInstance().getMongoManager().getPlayers().find(Filters.eq("uuid", uuid)).first();
            return playerDocument != null ? serverCore.getGson().fromJson(serverCore.getGson().toJson(playerDocument), User.class) : null;
        }
    }

    /**
     * Check if a player exists
     *
     * @param uuid
     * @return @{@link Boolean}
     */
    private boolean existsPlayer(String uuid) {
        return ServerCore.getInstance().getMongoManager().getPlayers().find(Filters.eq("uuid", uuid)).first() != null;
    }

    /**
     * Get the coins of a player from the database
     *
     * @param uuid
     * @return @{@link Integer}
     */
    public Integer getCoins(String uuid) {
        final Document document = ServerCore.getInstance().getMongoManager().findDocumentSync(ServerCore.getInstance().getMongoManager().getPlayers(), Filters.eq("uuid", uuid));
        final User user = serverCore.getGson().fromJson(serverCore.getGson().toJson(document), User.class);

        return Double.valueOf(user.getCoins()).intValue();
    }
}

package net.extasty.servercore.builder;

import com.google.common.collect.Maps;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 2019-05-11 um 14:24
 **/
public class ItemBuilder {

    private Material material;
    private int type;
    private int amount;
    private boolean unbreakable;
    private boolean hide;
    private Color color;
    private String displayName;
    private String[] lore;
    private int durability;
    private String skullOwner;
    private final Map<Enchantment, Integer> enchantmentMap;

    public ItemBuilder() {
        this.amount = 1;
        this.unbreakable = false;
        this.hide = false;
        this.displayName = "";
        this.durability = 0;
        this.enchantmentMap = Maps.newConcurrentMap();
    }

    public ItemBuilder(final Material material) {
        this.amount = 1;
        this.unbreakable = false;
        this.hide = false;
        this.displayName = "";
        this.durability = 0;
        this.enchantmentMap = Maps.newConcurrentMap();
        this.material = material;
    }

    public ItemBuilder(final int type) {
        this.amount = 1;
        this.unbreakable = false;
        this.hide = false;
        this.displayName = "";
        this.durability = 0;
        this.enchantmentMap = Maps.newConcurrentMap();
        this.type = type;
    }

    public ItemBuilder(final Material material, final int subID) {
        this(material);
        this.durability = subID;
    }

    public ItemBuilder(final int type, final int subID) {
        this(type);
        this.durability = subID;
    }

    public ItemBuilder(final Material material, final int subID, final String displayName) {
        this(material, subID);
        this.displayName = displayName;
    }

    public ItemBuilder(final int type, final int subID, final String displayName) {
        this(type, subID);
        this.displayName = displayName;
    }

    public ItemBuilder setType(final Material material) {
        this.material = material;
        return this;
    }

    public ItemBuilder setType(final int type) {
        this.type = type;
        return this;
    }

    public ItemBuilder addEnchantment(final Enchantment enchantment, final int level) {
        this.enchantmentMap.put(enchantment, level);
        return this;
    }

    public ItemBuilder setDisplayName(final String name) {
        this.displayName = name;
        return this;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public String[] getLore() {
        return this.lore;
    }

    public ItemBuilder setLore(final String[] lore) {
        this.lore = lore;
        return this;
    }

    public ItemBuilder setLoreAsString(final String... lore) {
        this.lore = lore;
        return this;
    }

    public ItemBuilder setAmount(final int amount) {
        this.amount = amount;
        return this;
    }

    public ItemBuilder setDurability(final short durability) {
        this.durability = durability;
        return this;
    }

    public ItemBuilder setSkullOwner(final String owner) {
        this.skullOwner = owner;
        return this;
    }

    public ItemBuilder setColor(final Color color) {
        this.color = color;
        return this;
    }

    public ItemBuilder hideEnchantments() {
        this.hide = !this.hide;
        return this;
    }

    public ItemBuilder setUnbreakable(final boolean unbreakable) {
        this.unbreakable = unbreakable;
        return this;
    }

    public ItemStack build() {
        ItemStack stack;
        if (this.material == null) {
            stack = new ItemStack(this.type, this.amount, (short) this.durability);
        } else {
            stack = new ItemStack(this.material, this.amount, (short) this.durability);
        }
        final ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(this.displayName);
        meta.spigot().setUnbreakable(this.unbreakable);
        if (this.hide) {
            meta.addItemFlags(new ItemFlag[]{ItemFlag.HIDE_ATTRIBUTES});
        }
        for (final Enchantment enchantments : this.enchantmentMap.keySet()) {
            meta.addEnchant(enchantments, (int) this.enchantmentMap.get(enchantments), true);
        }
        if (this.lore != null) {
            meta.setLore((List) Arrays.asList(this.lore));
        }
        if (this.skullOwner != null) {
            final SkullMeta skullMeta = (SkullMeta) meta;
            skullMeta.setOwner(this.skullOwner);
            stack.setItemMeta((ItemMeta) skullMeta);
        } else if (this.color != null) {
            final LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) meta;
            leatherArmorMeta.setColor(this.color);
            stack.setItemMeta((ItemMeta) leatherArmorMeta);
        } else {
            stack.setItemMeta(meta);
        }
        return stack;
    }
}

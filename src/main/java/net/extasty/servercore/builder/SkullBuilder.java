package net.extasty.servercore.builder;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 2019-05-17 um 20:02
 **/
public class SkullBuilder {

    private ItemStack itemStack;

    public SkullBuilder() {
        this.itemStack = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
    }

    public SkullBuilder(final Material material, final int amount, final int extra) {
        this.itemStack = new ItemStack(material, amount, (short) (byte) extra);
    }

    public SkullBuilder(final String name, final String lore, final String owner) {
        this.itemStack = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
        final SkullMeta meta = (SkullMeta) this.itemStack.getItemMeta();
        meta.setLore(Arrays.asList(lore));
        meta.setDisplayName(name);
        meta.setOwner(owner);
        this.itemStack.setItemMeta(meta);
    }

    public SkullBuilder(final Material material, final int amount, final int extra, final String name, final String lore) {
        this.itemStack = new ItemStack(material);
        final SkullMeta meta = (SkullMeta) this.itemStack.getItemMeta();
        meta.setLore(Arrays.asList(lore));
        meta.setDisplayName(name);
        this.itemStack.setItemMeta(meta);
    }

    public SkullBuilder setName(final String name) {
        final SkullMeta meta = (SkullMeta) this.itemStack.getItemMeta();
        meta.setDisplayName(name);
        this.itemStack.setItemMeta(meta);
        return this;
    }

    public SkullBuilder setOwner(final String owner) {
        final SkullMeta meta = (SkullMeta) this.itemStack.getItemMeta();
        meta.setOwner(owner);
        this.itemStack.setItemMeta(meta);
        return this;
    }

    public SkullBuilder addEnchantment(final Enchantment enchantment, final int level) {
        this.itemStack.addEnchantment(enchantment, level);
        return this;
    }

    public SkullBuilder setInfiniteDurability() {
        this.itemStack.setDurability((short) 32767);
        return this;
    }

    public SkullBuilder removeEnchantment(final Enchantment enchantment) {
        this.itemStack.removeEnchantment(enchantment);
        return this;
    }

    public SkullBuilder setAmount(final int amount) {
        this.itemStack.setAmount(amount);
        return this;
    }


    public SkullBuilder setLore(final String... lore) {
        final ItemMeta im = this.itemStack.getItemMeta();
        im.setLore(Arrays.asList(lore));
        this.itemStack.setItemMeta(im);
        return this;
    }

    public SkullBuilder setLore(final ArrayList<String> lore) {
        final ItemMeta im = this.itemStack.getItemMeta();
        im.setLore(lore);
        this.itemStack.setItemMeta(im);
        return this;
    }

    public ItemStack build() {
        return this.itemStack;
    }
}

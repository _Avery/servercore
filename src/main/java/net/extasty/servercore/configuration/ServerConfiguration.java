package net.extasty.servercore.configuration;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 2019-06-15 um 19:17
 **/

@Data
public class ServerConfiguration {

    @SerializedName("mongoUser")
    private String mongoUser;

    @SerializedName("mongoPassword")
    private String mongoPassword;

    @SerializedName("mongoDatabase")
    private String mongoDatabase;

    @SerializedName("mongoHostname")
    private String mongoHostname;

    @SerializedName("mongoPort")
    private int mongoPort;

    @SerializedName("disableChat")
    private boolean disableChat;

    /**
     * This method saves the configuration
     *
     * @param file
     * @throws IOException
     */
    public void save(final File file) throws IOException {
        final FileWriter writer = new FileWriter(file);

        writer.write(new GsonBuilder().setPrettyPrinting().create().toJson(this));
        writer.flush();
        writer.close();
    }
}

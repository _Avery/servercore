package net.extasty.servercore.misc.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 2019-06-15 um 19:38
 **/

@Data
@NoArgsConstructor
public class Settings {

    /* the uuid of the player */
    private String uuid;

    /* the settings */
    private boolean autoNick;
    private boolean doubleJump;
    private boolean jumpPlates;
    private boolean particles;

    /* visibility for the eg. the lobby */
    private int visibility;

}

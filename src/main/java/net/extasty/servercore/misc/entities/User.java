package net.extasty.servercore.misc.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 2019-06-08 um 22:14
 **/

@Data
@NoArgsConstructor
public class User {

    /* personal data */
    private String name;
    private String uuid;
    private long coins;
    private String ipAddress;

    /* some numbers */
    private long createdAt;
    private long lastJoin;
    private long onlineTime;

    /* clan */
    private String clanID;

    /* notify and privacy */
    private boolean notify;
    private boolean dataPrivacy;

    /* ban & mutes */
    private int banPoints;
    private int mutePoints;

}

package net.extasty.servercore.misc;

import de.dytanic.cloudnet.api.CloudAPI;
import net.extasty.servercore.ServerCore;
import org.bukkit.Color;
import org.bukkit.entity.Player;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 2019-06-16 um 16:48
 **/
public class Methods {

    private final ServerCore serverCore;

    public Methods(ServerCore serverCore) {
        this.serverCore = serverCore;
    }

    /**
     * This method formats an integer
     *
     * @param toFormat
     * @return @{@link String}
     */
    public String format(final int toFormat) {
        final String pointsString = String.valueOf(toFormat);
        final String[] pointsSplit = pointsString.split("");
        if (toFormat < 1000) {
            return String.valueOf(toFormat);
        }
        if (toFormat < 10000) {
            return pointsSplit[0] + "." + pointsSplit[1] + pointsSplit[2] + pointsSplit[3];
        }
        if (toFormat < 100000) {
            return pointsSplit[0] + pointsSplit[1] + "." + pointsSplit[2] + pointsSplit[3] + pointsSplit[4];
        }
        if (toFormat < 1000000) {
            return pointsSplit[0] + pointsSplit[1] + pointsSplit[2] + "." + pointsSplit[3] + pointsSplit[4] + pointsSplit[5];
        }
        if (toFormat < 10000000) {
            return pointsSplit[0] + "." + pointsSplit[1] + pointsSplit[2] + pointsSplit[3] + "." + pointsSplit[4] + pointsSplit[5] + pointsSplit[6];
        }
        if (toFormat < 100000000) {
            return pointsSplit[0] + pointsSplit[1] + "." + pointsSplit[2] + pointsSplit[3] + pointsSplit[4] + "." + pointsSplit[5] + pointsSplit[6] + pointsSplit[7];
        }
        if (toFormat < 1000000000) {
            return pointsSplit[0] + pointsSplit[1] + pointsSplit[2] + "." + pointsSplit[3] + pointsSplit[4] + pointsSplit[5] + "." + pointsSplit[6] + pointsSplit[7] + pointsSplit[8];
        }
        return String.valueOf(toFormat);
    }

    /**
     * Get the player permission group
     *
     * @param player
     * @return @{@link PermissionGroup}
     */
    public PermissionGroup getPlayerGroup(final Player player) {
        return PermissionGroup.valueOf(CloudAPI.getInstance().getOnlinePlayer(
                player.getUniqueId()).getPermissionEntity().getGroups().iterator().next().getGroup().toUpperCase());
    }

    /**
     * Get armor color
     *
     * @param player
     * @return @{@link Color}
     */
    public Color getArmorColor(final Player player) {
        final PermissionGroup permissionGroup = PermissionGroup.valueOf(CloudAPI.getInstance().getOnlinePlayer(
                player.getUniqueId()).getPermissionEntity().getGroups().iterator().next().getGroup().toUpperCase());

        return permissionGroup.getRgbColor();
    }
}

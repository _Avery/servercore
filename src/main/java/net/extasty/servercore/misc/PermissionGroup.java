package net.extasty.servercore.misc;

import lombok.Getter;
import org.bukkit.Color;

import java.beans.ConstructorProperties;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 2019-06-15 um 15:21
 **/

@Getter
public enum PermissionGroup {

    ADMIN("Administrator", "§4", "§4Admin §8┃ ", "§4Admin §8× §4", "0000Admin", Color.RED),
    DEVELOPER("Developer", "§c", "§cDev §8┃ ", "§cDev §8× §c", "0001Dev", Color.fromBGR(255, 85, 85)),
    MODERATOR("Moderator", "§5", "§5Mod §8┃ ", "§5Mod §8× §5", "0002Mod", Color.fromBGR(170, 0, 170)),
    SUPPORTER("Supporter", "§9", "§9Sup §8┃ ", "§9Sup §8× §9", "0003Sup", Color.fromBGR(85, 85, 255)),
    CREATOR("Creator", "§3", "§3Creator §8┃ ", "§3Creator §8× §3", "0004Creator", Color.fromBGR(0, 170, 170)),
    BUILDER("Builder", "§b", "§bBuilder §8┃ ", "§bBuilder §8× §b", "0005Builder", Color.fromBGR(85, 255, 255)),
    VIP_PLUS("VIP+", "§d", "§dVIP+ §8┃ ", "§dVIP+ §8× §d", "0006VIPPlus", Color.fromBGR(255, 85, 255)),
    VIP("VIP", "§d", "§dVIP §8┃ ", "§dVIP §8× §d", "0007VIP", Color.fromBGR(255, 85, 255)),
    PRIME("Prime", "§a", "§a", "§a", "0008Prime", Color.fromBGR(85, 255, 85)),
    PLAYER("Spieler", "§7", "§7", "§7", "0009Spieler", Color.fromBGR(170, 170, 170));

    private final String group;
    private final String color;
    private final String chatPrefix;
    private final String tabPrefix;
    private final String tabTeam;
    private final Color rgbColor;

    public static PermissionGroup getGroupByName(String name) {
        PermissionGroup[] permissionGroups = values();

        for (int counter = 0; counter < permissionGroups.length; ++counter) {
            PermissionGroup eachGroup = permissionGroups[counter];
            if (eachGroup.getGroup().equalsIgnoreCase(name)) {
                return eachGroup;
            }
        }

        return PLAYER;
    }

    @ConstructorProperties({"group", "color", "chatPrefix", "tabPrefix", "tabTeam"})
    PermissionGroup(String group, String color, String chatPrefix, String tabPrefix, String tabTeam, Color rgbColor) {
        this.group = group;
        this.color = color;
        this.chatPrefix = chatPrefix;
        this.tabPrefix = tabPrefix;
        this.tabTeam = tabTeam;
        this.rgbColor = rgbColor;
    }
}


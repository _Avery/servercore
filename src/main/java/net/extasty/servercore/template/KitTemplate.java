package net.extasty.servercore.template;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 2019-07-20 um 19:40
 **/

@Getter
@AllArgsConstructor
public abstract class KitTemplate {

    private String name;
    private String displayName;
    private ArrayList<ItemStack> items;
    private ArrayList<ItemStack> armor;
    private ItemStack symbol;

    /**
     * This method sets the player the items in inventories
     * <p>
     * INFO:
     * The armor must be set in an arraylist (order boots -> helmet)
     *
     * @param player
     */
    public void setPlayerItems(final Player player) {
        player.getInventory().clear();

        ItemStack[] armorStack = armor.toArray(new ItemStack[armor.size()]);

        this.items.forEach(all -> player.getInventory().addItem(all));
        this.armor.forEach(all -> player.getInventory().setArmorContents(armorStack));
    }
}

package net.extasty.servercore;

import com.google.gson.Gson;
import lombok.Getter;
import net.extasty.servercore.configuration.ServerConfiguration;
import net.extasty.servercore.eventlisteners.AsyncPlayerChatListener;
import net.extasty.servercore.eventlisteners.PlayerJoinListener;
import net.extasty.servercore.eventlisteners.PlayerQuitListener;
import net.extasty.servercore.managers.BackendManager;
import net.extasty.servercore.managers.MongoManager;
import net.extasty.servercore.managers.RedisManager;
import net.extasty.servercore.managers.game.KitManager;
import net.extasty.servercore.managers.spectator.SpectatorManager;
import net.extasty.servercore.misc.Methods;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 2019-06-08 um 22:00
 **/

@Getter
public class ServerCore extends JavaPlugin {

    @Getter
    private static ServerCore instance;

    private Gson gson;
    private PluginManager pluginManager;
    private ServerConfiguration serverConfiguration;
    private File configFile;

    private Methods methods;
    private BackendManager backendManager;
    private MongoManager mongoManager;
    private KitManager kitManager;
    private SpectatorManager spectatorManager;
    private RedisManager redisManager;

    @Override
    public void onLoad() {
        /* set the instance */
        instance = this;
    }

    @Override
    public void onEnable() {
        /* create config */
        this.createConfig();

        /* fetch classes */
        this.fetchClasses();

        /* register listener */
        this.registerListener();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    /**
     * This method fetch all classes
     */
    private void fetchClasses() {
        this.gson = new Gson();
        this.pluginManager = Bukkit.getPluginManager();

        this.mongoManager = new MongoManager(this.serverConfiguration.getMongoHostname(),
                this.serverConfiguration.getMongoDatabase(), this.serverConfiguration.getMongoPort(),
                this.serverConfiguration.getMongoUser(), this.serverConfiguration.getMongoPassword());
        this.mongoManager.connect((success) -> {
            this.getServer().getConsoleSender().sendMessage(success ? "§aMongoDB connection established!" : "§cAn error occurred while connecting to MongoDB Server!");
        });

        this.redisManager = new RedisManager(this);
        this.redisManager.connect();

        this.methods = new Methods(this);
        this.backendManager = new BackendManager(this);
        this.kitManager = new KitManager();
        this.spectatorManager = new SpectatorManager(this);
    }

    /**
     * This method registers all listeners
     */
    private void registerListener() {
        if (!this.serverConfiguration.isDisableChat())
            this.pluginManager.registerEvents(new AsyncPlayerChatListener(this), this);

        this.pluginManager.registerEvents(new PlayerJoinListener(this), this);
        this.pluginManager.registerEvents(new PlayerQuitListener(this), this);
    }

    /**
     * Create the config file
     */
    private void createConfig() {
        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
        }

        configFile = new File(getDataFolder(), "data.json");

        try {
            if (configFile.exists()) {
                serverConfiguration = new Gson().fromJson(new FileReader(configFile), ServerConfiguration.class);
            } else {
                configFile.createNewFile();
                serverConfiguration = new ServerConfiguration();
                serverConfiguration.save(configFile);
            }
        } catch (final IOException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Set meta data
     *
     * @param entity
     * @param key
     * @param value
     */
    public void setMetadata(final Entity entity, final String key, final Object value) {
        this.removeMetadata(entity, key);
        entity.setMetadata(key, new FixedMetadataValue(this, value));
    }

    /**
     * Set meta data
     *
     * @param block
     * @param key
     * @param value
     */
    public void setMetadata(final Block block, final String key, final Object value) {
        this.removeMetadata(block, key);
        block.setMetadata(key, new FixedMetadataValue(this, value));
    }

    /**
     * Remove meta data
     *
     * @param entity
     * @param key
     */
    public void removeMetadata(final Entity entity, final String key) {
        if (entity.hasMetadata(key)) {
            entity.removeMetadata(key, this);
        }
    }

    /**
     * Remove meta data
     *
     * @param block
     * @param key
     */
    public void removeMetadata(final Block block, final String key) {
        if (block.hasMetadata(key)) {
            block.removeMetadata(key, this);
        }
    }

    /**
     * Run bukkit task
     *
     * @param runnable
     * @return @{@link BukkitTask}
     */
    public BukkitTask runTask(Runnable runnable) {
        return this.getServer().getScheduler().runTask(this, runnable);
    }

    /**
     * Run bukkit task async
     *
     * @param runnable
     * @return @{@link BukkitTask}
     */
    public BukkitTask runTaskAsync(Runnable runnable) {
        return this.getServer().getScheduler().runTaskAsynchronously(this, runnable);
    }

    /**
     * Run bukkit task later
     *
     * @param delay
     * @param runnable
     * @return @{@link BukkitTask}
     */
    public BukkitTask runTaskLater(long delay, Runnable runnable) {
        return this.getServer().getScheduler().runTaskLater(this, runnable, delay);
    }

    /**
     * Run bukkit task later async
     *
     * @param delay
     * @param runnable
     * @return @{@link BukkitTask}
     */
    public BukkitTask runTaskLaterAsync(long delay, Runnable runnable) {
        return this.getServer().getScheduler().runTaskLaterAsynchronously(this, runnable, delay);
    }


    /**
     * Run bukkit task timer
     *
     * @param delay
     * @param repeat
     * @param runnable
     * @return @{@link BukkitTask}
     */
    public BukkitTask runTaskTimer(long delay, long repeat, Runnable runnable) {
        return this.getServer().getScheduler().runTaskTimer(this, runnable, delay, repeat);
    }
}
